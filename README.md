# TEEM #



### What is TEEM? ###

**TEEM** (**T**chibo **E**commerce **E**xperience & **M**arketplace) is a virtual shopping ecosystem for **Tchibo**.

**TEEM** starts as a virtual 3D representation of a **Tchibo** coffee shop with the following components:
 
- Interactive 3D assets including a **Tchibo** coffee machine that you can operate virtually

- Integrated ecommerce apps including a **TEEM** Magento webshop and a **TEEM** PWA that upgrades the **Tchibo** mobile app  

- Access to inventory of coffee & machines, fashion accessories, sportswear & equipment, and home & garden furniture 

- **TEEM Card** which updates the **TchiboCard**   